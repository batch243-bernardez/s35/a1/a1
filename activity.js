const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://mpyb:mpyb@zuittbatch243.kotcmj5.mongodb.net/?retryWrites=true&w=majority", 
	{ 
		useNewUrlParser : true,  
		useUnifiedTopology : true
	}
);


// Create a User schema.
	const userSchema = new mongoose.Schema({ 
			username : String,
			password : String
		});


// Create a User model.
	const User = mongoose.model("User", userSchema);

	let db = mongoose.connection;
	db.on("error", console.error.bind(console, "connection error"));
	db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({extended:true}))


// Create a POST route that will access the /signup route that will create a user.
	app.post("/signup", (req,res) => {
		User.findOne({username: req.body.username}, (err,result) => {
			if(result != null && result.username == req.body.username){
				return res.send("Username already been taken!");
			}

			else{
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				});

// Process a POST request at the /signup route using postman to register a user.
				newUser.save((saveErr, savedTask) => {
					if(saveErr){
						return console.error(saveErr);
					}

					else{
						return res.status(201).send("New user registered");
					}
				})
			}
		})
	})

app.listen(port, () => console.log(`Server running at port ${port}`));


